
set(SHARED_SOURCES
    shared/CommandParser.cpp
    shared/CommandParser.h
    shared/CommandParserHistory.cpp
    shared/CommandParserHistory.h
    shared/CommandPermissionManager.cpp
    shared/CommandPermissionManager.h
    shared/FirstSharedCommandParser.h
)

if(WIN32)
    set(PLATFORM_SOURCES
        win32/FirstSharedCommandParser.cpp
    )   
endif()

include_directories(
    ${SWG_ENGINE_SOURCE_DIR}/shared/library/sharedCommandParser/include/public
    ${SWG_ENGINE_SOURCE_DIR}/shared/library/sharedDebug/include/public
    ${SWG_ENGINE_SOURCE_DIR}/shared/library/sharedFoundation/include/public
    ${SWG_ENGINE_SOURCE_DIR}/shared/library/sharedFoundationTypes/include/public
    ${SWG_ENGINE_SOURCE_DIR}/shared/library/sharedMemoryManager/include/public
    ${SWG_EXTERNALS_SOURCE_DIR}/ours/library/unicode/include
)

add_shared_library(NAME sharedCommandParser SOURCES ${SHARED_SOURCES} ${PLATFORM_SOURCES})
