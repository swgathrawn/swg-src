
set(SHARED_SOURCES
    shared/BitStream.cpp
    shared/BitStream.h
    shared/Compressor.cpp
    shared/Compressor.h
    shared/FirstSharedCompression.h
    shared/Lz77.cpp
    shared/Lz77.h
    shared/SetupSharedCompression.cpp
    shared/SetupSharedCompression.h
    shared/ZlibCompressor.cpp
    shared/ZlibCompressor.h
)

if(WIN32)
    set(PLATFORM_SOURCES
        win32/FirstSharedCompression.cpp
    )
endif()

include_directories(
    ${SWG_ENGINE_SOURCE_DIR}/shared/library/sharedCompression/include/public
    ${SWG_ENGINE_SOURCE_DIR}/shared/library/sharedDebug/include/public
    ${SWG_ENGINE_SOURCE_DIR}/shared/library/sharedFoundation/include/public
    ${SWG_ENGINE_SOURCE_DIR}/shared/library/sharedFoundationTypes/include/public
    ${SWG_ENGINE_SOURCE_DIR}/shared/library/sharedMemoryManager/include/public
    ${SWG_ENGINE_SOURCE_DIR}/shared/library/sharedSynchronization/include/public
    ${SWG_EXTERNALS_SOURCE_DIR}/3rd/library/zlib-1.2.3
)

add_shared_library(NAME sharedCompression SOURCES ${SHARED_SOURCES} ${PLATFORM_SOURCES})
