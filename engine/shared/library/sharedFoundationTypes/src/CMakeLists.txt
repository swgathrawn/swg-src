
set(SHARED_SOURCES shared/FoundationTypes.h)

if(WIN32)
    set(PLATFORM_SOURCES win32/FoundationTypesWin32.h)
else()
    set(PLATFORM_SOURCES linux/FoundationTypesLinux.h)
endif()

add_shared_library(NAME sharedFoundationTypes SOURCES ${SHARED_SOURCES} ${PLATFORM_SOURCES})

set_target_properties(sharedFoundationTypes PROPERTIES LINKER_LANGUAGE CXX)
