
include_directories(
	${SWG_EXTERNALS_SOURCE_DIR}/3rd/library/udplibrary
    ${SWG_EXTERNALS_SOURCE_DIR}/3rd/library/zlib-1.2.3
)

add_library(MonAPI2
	MonitorAPI.cpp
	MonitorAPI.h
	MonitorData.cpp
	MonitorData.h
)
