
set(SHARED_SOURCES
    hashtable.hpp
    PointerDeque.hpp
    priority.hpp
    UdpHandler.h
    UdpHandler.hpp
    UdpLibrary.cpp
    UdpLibrary.h
    UdpLibrary.hpp
)

add_shared_library(NAME udplibrary SOURCES ${SHARED_SOURCES})
