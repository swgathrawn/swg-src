
if (${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
    message(WARNING "\n\nCMake generation is not allowed within the source directory!\n")
    message(STATUS "Cancelling CMake and cleaning up source tree...")
    execute_process(COMMAND ${CMAKE_COMMAND} -E remove_directory "${CMAKE_BINARY_DIR}/CMakeFiles")
    math(EXPR Crash 0/0)
    message(FATAL_ERROR "CMake should have crashed - this is a failsafe in case the call used to trigger the crash gets fixed.")
endif()

cmake_minimum_required(VERSION 3.0)

project(swg C CXX)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

include(CustomBuildTargets)

set(SWG_ROOT_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(SWG_ENGINE_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/engine)
set(SWG_EXTERNALS_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/external)
set(SWG_GAME_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/game)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/bin)

if ("x${CMAKE_CXX_COMPILER_ID}" STREQUAL "xMSVC")
    add_definitions(/D_SILENCE_STDEXT_HASH_DEPRECATION_WARNINGS /D_CRT_SECURE_NO_WARNINGS)
    add_definitions(/wd4091 /wd4244 /wd4477 /wd4996)
endif()

if(WIN32)
    set(ENV{PATH} "$ENV{PATH};${SWG_EXTERNALS_SOURCE_DIR}/3rd/bin")
    set(ICONV_ROOT "${SWG_EXTERNALS_SOURCE_DIR}/3rd/library/libiconv")
    set(LIBXML2_ROOT "${SWG_EXTERNALS_SOURCE_DIR}/3rd/library/libxml2")
    set(PCRE_ROOT "${SWG_EXTERNALS_SOURCE_DIR}/3rd/library/pcre/4.1/win32")
    set(ORACLE_ROOT "${SWG_EXTERNALS_SOURCE_DIR}/3rd/library/instantclient_12_1/sdk")
    set(ZLIB_ROOT "${SWG_EXTERNALS_SOURCE_DIR}/3rd/library/zlib")

    find_package(Iconv REQUIRED)
endif()

find_package(BISON REQUIRED)
find_package(FLEX REQUIRED)
find_package(JNI REQUIRED)
find_package(LibXml2 REQUIRED)
find_package(Oracle REQUIRED)
find_package(PCRE REQUIRED)
find_package(Perl REQUIRED)

add_subdirectory(engine)
add_subdirectory(external)
add_subdirectory(game)

add_build_target_groups()
