#
# Adds a set of wrappers for CMake's add_library/add_executable functions that adds
# project type specific modifiers.
#

macro(create_custom_build_target label target_type)
    set(cach_var_name "_all_${label}_${target_type}")
    unset(${cach_var_name} CACHE)
    set(func_name "add_${label}_${target_type}")

    function(${func_name})
        set(func_args ARGN)
        set(cach_var_name "_all_${label}_${target_type}")
        set(target_type ${target_type})

        cmake_parse_arguments(PARSED_ARGS "" "NAME" "SOURCES" ${${func_args}})
    
        if (target_type STREQUAL "library")
            add_library(${PARSED_ARGS_NAME} ${PARSED_ARGS_SOURCES})
        else()
            add_executable(${PARSED_ARGS_NAME} ${PARSED_ARGS_SOURCES})
        endif()

        set(${cach_var_name} ${${cach_var_name}} ${PARSED_ARGS_NAME} CACHE INTERNAL "${label} ${target_type}")
    endfunction()
endmacro()

create_custom_build_target(client executable)
create_custom_build_target(client library)
create_custom_build_target(server executable)
create_custom_build_target(server library)
create_custom_build_target(shared library)
create_custom_build_target(tool executable)

function(add_build_target_groups)
    add_custom_target(_all_client DEPENDS ${_all_client_executable})
    add_custom_target(_all_client_libraries DEPENDS ${_all_client_library})
    add_custom_target(_all_server DEPENDS ${_all_server_executable})
    add_custom_target(_all_server_libraries DEPENDS ${_all_server_library})
    add_custom_target(_all_shared_libraries DEPENDS ${_all_shared_library})
    add_custom_target(_all_tools DEPENDS ${_all_tool_executable})
endfunction()
