
find_path(ORACLE_INCLUDE_DIR
	PATHS ${ORACLE_ROOT}/include
 	NAMES oci.h )
	
set(ORACLE_OCI_NAMES clntsh libclntsh oci)
set(ORACLE_NNZ_NAMES nnz10 libnnz10 nnz11 libnnz11 nnz12 libnnz12 ociw32)
set(ORACLE_OCCI_NAMES libocci occi oraocci10 oraocci11 oraocci12)

set(ORACLE_LIB_DIR 
  ${ORACLE_ROOT}/lib/msvc)


find_library(ORACLE_LIBRARY  NAMES ${ORACLE_OCI_NAMES} PATHS ${ORACLE_LIB_DIR})
find_library(ORACLE_OCCI_LIBRARY NAMES ${ORACLE_OCCI_NAMES} PATHS ${ORACLE_LIB_DIR})
find_library(ORACLE_NNZ_LIBRARY NAMES ${ORACLE_NNZ_NAMES} PATHS ${ORACLE_LIB_DIR})
  

# handle the QUIETLY and REQUIRED arguments and set OPENAL_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ORACLE DEFAULT_MSG ORACLE_LIBRARY ORACLE_INCLUDE_DIR)

mark_as_advanced(ORACLE_INCLUDE_DIR ORACLE_LIBRARY ORACLE_ROOT)

